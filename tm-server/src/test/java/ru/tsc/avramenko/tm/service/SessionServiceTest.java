package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.SessionRepository;
import ru.tsc.avramenko.tm.repository.UserRepository;

import java.util.Date;
import java.util.List;

public class SessionServiceTest {

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Session session;

    @Nullable
    private User user = new User();;

    @NotNull
    protected static final String TEST_USER_LOGIN = "TestLogin";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_PASSWORD = "TestUserPassword";

    @NotNull
    protected static final String TEST_USER_PASSWORD_INCORRECT = "TestUserPasswordIncorrect";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @Before
    public void before() {
        user = bootstrap.getUserService().create(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        sessionService = new SessionService(new SessionRepository(), bootstrap);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        this.session = sessionService.add(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals(user.getId(), session.getUserId());

        @NotNull final Session sessionById = sessionService.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionService.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionService.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        sessionService.findById(null);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionService.findAll(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionService.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test(expected = EmptyIdException.class)
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionService.findAll(null);
        Assert.assertNull(session);
    }

    @Test
    public void remove() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        sessionService.removeById(null);
    }

    @Test
    public void removeById() {
        sessionService.removeById(session.getId());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        sessionService.removeById(null);
    }

    @Test
    public void removeByIdIncorrect() {
        @NotNull final Session session = sessionService.removeById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test
    public void open() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void openIncorrect() {
        sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD_INCORRECT);
        Assert.assertNotNull(session);
    }

    @Test
    public void validate() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        sessionService.validate(session);
        Assert.assertNotNull(session);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateChanged() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        session.setSignature("647");
        sessionService.validate(session);
    }

    @Test
    public void close() {
        @Nullable final Session session = sessionService.open(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        sessionService.close(session);
        Assert.assertNull(sessionService.findById(session.getId()));
    }

}