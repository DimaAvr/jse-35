package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Session;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @Before
    public void before() {
        sessionRepository = new SessionRepository();
        @NotNull final Session session = new Session();
        session.setUserId(TEST_USER_ID);
        this.session = sessionRepository.add(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertEquals(TEST_USER_ID, session.getUserId());

        @NotNull final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session, sessionById);
    }

    @Test
    public void findById() {
        @NotNull final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @NotNull final Session session = sessionRepository.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test
    public void findByIdNull() {
        @NotNull final Session session = sessionRepository.findById(null);
        Assert.assertNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAll(this.session.getUserId());
        Assert.assertNotNull(session);
        Assert.assertEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Session> session = sessionRepository.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotNull(session);
        Assert.assertNotEquals(1, session.size());
    }

    @Test
    public void findAllByUserIdNull() {
        @NotNull final List<Session> session = sessionRepository.findAll(null);
        Assert.assertTrue(session.isEmpty());
    }

    @Test
    public void remove() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

    @Test
    public void removeById() {
        sessionRepository.removeById(session.getId());
        Assert.assertNull(sessionRepository.findById(session.getId()));
    }

    @Test
    public void removeByIdNull() {
        Assert.assertNull(sessionRepository.removeById(null));
    }

    @Test
    public void removeByIdIncorrect() {
        @Nullable final Session session = sessionRepository.removeById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

}